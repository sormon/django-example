from django.conf.urls import url

from . import views

urlpatterns = [
    url(r"^$", views.main_board, name="main_board"),
    url(r"^main_board$", views.main_board, name="main_board"),
    url(r"^partners$", views.partners, name="partners"),
    url(r"^expends$", views.expenditures, name="expend")
]

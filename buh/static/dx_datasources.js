var base_url = "/buh/api";

op_types = [
    {
        "id": "OUT",
        "name": "Outcome"
    },
    {
        "id": "IN",
        "name": "Income"
    }
];

partners = new DevExpress.data.DataSource({
    load: function (loadOptions) {
        var d = $.Deferred();
        $.getJSON(base_url + "/partners")
            .done(function (result) {
                d.resolve(result.results);
            });
        return d.promise();
    },
    insert: function(values){
        var d = $.Deferred();
        $.ajax({
            url: base_url + "/partners/",
            type: "POST",
            beforeSend: function(request) {
                request.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            },
            data: JSON.stringify(values),
            contentType: 'application/json; charset=utf-8',
            success: function(data, status, jqxhr){
                d.resolve(data);
            }
        });
        return d.promise();
    }
});

expendids = new DevExpress.data.DataSource({
    load: function (loadOptions) {
        var d = $.Deferred();
        $.getJSON(base_url + "/expendids")
            .done(function (result) {
                d.resolve(result.results);
            });
        return d.promise();
    },
    insert: function(values){
        var d = $.Deferred();
        $.ajax({
            url: base_url + "/expendids/",
            type: "POST",
            beforeSend: function(request) {
                request.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            },
            data: JSON.stringify(values),
            contentType: 'application/json; charset=utf-8',
            success: function(data, status, jqxhr){
                d.resolve(data);
            }
        });
        return d.promise();
    }
});

mainBoardDatasource = new DevExpress.data.DataSource({
    paginate: true,
    // pageSize: 5,
    load: function (loadOptions) {
        var d = $.Deferred();
        var args = {};
        console.log(loadOptions);
        if (loadOptions.sort) {
            var sorts = [];
            for (i in loadOptions.sort){
                var s = loadOptions.sort[i];
                // Django REST OrderingFilter style
                sorts.push((s.desc ? "-" : "") + s.selector);
            }
            args.ordering = sorts.join(",");
        }
        args.offset = loadOptions.skip;
        args.limit = loadOptions.take;
        $.getJSON(base_url + "/mainboard", args).done(function (result) {
            d.resolve(result.results, {
                totalCount: result.count
            });
        });
        return d.promise();
    },
    insert: function(values){
        var d = $.Deferred();
        $.ajax({
            url: base_url + "/mainboard/",
            type: "POST",
            beforeSend: function(request) {
                request.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            },
            data: JSON.stringify(values),
            contentType: "application/json; charset=utf-8",
            success: function(data, status, jqxhr){
                d.resolve(data);
            }
        });
        return d.promise();
    }
});
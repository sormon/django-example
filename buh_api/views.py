# from django.http import HttpResponse
# from django.core import serializers
# from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from rest_framework.filters import OrderingFilter
from . import serializers as serials
from . import models


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    permission_classes = (IsAdminUser, )
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = serials.UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    permission_classes = (IsAdminUser,)
    queryset = Group.objects.all()
    serializer_class = serials.GroupSerializer


class MainBoardViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    permission_classes = (DjangoModelPermissionsOrAnonReadOnly,)
    queryset = models.MainBoard.objects.all()
    serializer_class = serials.MainBoardSerializer
    filter_backends = (OrderingFilter,)
    ordering_fields = '__all__'
    ordering = ('-op_date',)


class ExpenditureViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    permission_classes = (DjangoModelPermissionsOrAnonReadOnly,)
    queryset = models.Expenditure.objects.all()
    serializer_class = serials.ExpenditureSerializer
    filter_backends = (OrderingFilter,)


class PartnerViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    permission_classes = (DjangoModelPermissionsOrAnonReadOnly,)
    queryset = models.Partner.objects.all()
    serializer_class = serials.PartnerSerializer
    filter_backends = (OrderingFilter,)


# @csrf_exempt
# def api_main_board(request):
#     if request.method == "POST":
#         print(request.content_type)
#         print(request.body)
#         try:
#             j = serializers.deserialize("json", request.body)
#             print(j)
#             for i in j:
#                 print(i)
#                 i.save()
#         except Exception as e:
#             print(e)
#             raise
#     return HttpResponse(
#         serializers.serialize(
#             "json",
#             models.MainBoard.objects.all()
#         ),
#         content_type="application/json"
#     )


# @csrf_exempt
# def api_partner(request):
#     if request.method == "POST":
#         print(request.content_type)
#         print(request.body)
#         try:
#             j = serializers.deserialize("json", request.body)
#             print(j)
#             for i in j:
#                 print(i)
#                 i.save()
#         except Exception as e:
#             print(e)
#             raise
#     return HttpResponse(
#         serializers.serialize(
#             "json",
#             models.Partner.objects.all()
#         ),
#         content_type="application/json"
#     )


# @csrf_exempt
# def api_expend(request):
#     if request.method == "POST":
#         print(request.content_type)
#         print(request.body)
#         try:
#             j = serializers.deserialize("json", request.body)
#             print(j)
#             for i in j:
#                 print(i)
#                 i.save()
#         except Exception as e:
#             print(e)
#             raise
#     return HttpResponse(
#         serializers.serialize(
#             "json",
#             models.Expenditure.objects.all()
#         ),
#         content_type="application/json"
#     )

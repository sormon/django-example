# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


class Expenditure(models.Model):
    name = models.CharField(unique=True, max_length=200)

    def __str__(self):
        return self.name


class Partner(models.Model):
    name = models.CharField(unique=True, max_length=200)

    def __str__(self):
        return self.name


class MainBoard(models.Model):
    op_date = models.DateField()
    op_type = models.CharField(
        max_length=3, choices=(("IN", "Income"), ("OUT", "Outcome")))
    op_expend = models.ForeignKey(
        Expenditure, null=True, blank=False, on_delete=models.SET_NULL)
    partner = models.ForeignKey(
        Partner, null=True, blank=False, on_delete=models.SET_NULL)
    op_balance = models.DecimalField(
        max_digits=9, decimal_places=2, blank=False)
    note = models.TextField()
    
    def __str__(self):
        return f"{self.op_date}: {self.op_type} "\
               f"{self.op_balance} ({self.note})"\
               f" {self.partner.name} {self.op_expend.name}"

from django.conf.urls import url
from django.conf.urls import include
from rest_framework import routers
from . import views


router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'mainboard', views.MainBoardViewSet)
router.register(r'partners', views.PartnerViewSet)
router.register(r'expendids', views.ExpenditureViewSet)


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include(
        'rest_framework.urls', namespace='rest_framework')),
    # url(r"^mainboard$", views.api_main_board, name="api_main_board"),
    # url(r"^partners$", views.api_partner, name="api_partner"),
    # url(r"^expendids$", views.api_expend, name="api_expend"),
]
from django.apps import AppConfig


class BuhApiConfig(AppConfig):
    name = 'buh_api'
